# metaSPARSim

metaSPARSim is an R tool for 16S rRNA-gene sequencing count data simulation.


## Installation

#### Installation from GitLab

metaSPARSim is available on GitLab at https://gitlab.com/sysbiobig/metaSPARSim

To install metaSPARSim from GitLab, please use the following commands:
```r
library(devtools)
install_gitlab("sysbiobig/metaSPARSim", build_opts = c("--no-resave-data", "--no-manual"), build_vignettes = TRUE)
```
The above commands would install metaSPARSim, the required dependencies and metaSPARSim vignette. 

To install metaSPARSim without the vignette, please use the following commands:
```r
library(devtools)
install_gitlab("sysbiobig/metaSPARSim")
```



#### Installation from source package

metaSPARSim R package can be downloaded at http://sysbiobig.dei.unipd.it/?q=metaSPARSim

It requires *RCpp*, *scran* and *edgeR* packages to work.

To install it from source, please use the following command:
```r
install.packages("metaSPARSim_X.X.X.tar.gz", repos = NULL, type = "source")
```
where "X.X.X" indicates the package version.

## Getting started

Please browse metaSPARSim vignette using the following commands:

```r
library(metaSPARSim)
browseVignettes("metaSPARSim")
```
